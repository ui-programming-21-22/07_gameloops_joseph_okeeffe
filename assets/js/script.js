const canvas = document.querySelector("#theCanvas");
const context = canvas.getContext("2d");



window.addEventListener("keydown", input);
window.addEventListener("keyup", input);
window.addEventListener("scroll", input);

let clockText = document.getElementById("clock");

let keyImage = new Image();
let dinoImage = new Image();

let playerInput = new PlayerInput("None");

let posX = 200;
let posY = 0;

let dinoPosX = 1200;
let dinoPosY = 0;
let dinoSizeX = 400;
let dinoSizeY = 700;

let scoreText = document.getElementById("score");
let score = 0;

let grow = false;
let playerSize = 250;
let playerRotation = 45;


function updateTime()
{
    var date  = new Date();
    
    clockText.innerHTML = ("Year: " +date.getFullYear() +
    " <br> Month: " + (date.getMonth() + 1) +
    " <br> Date: " + date.getDate() +
    " <br>Hour: " + date.getHours() +
    " <br> Minutes: " + date.getMinutes() +
    " <br>Seconds: " + date.getSeconds())  
    window.requestAnimationFrame(updateTime);
    

   
   

   
                
}

window.requestAnimationFrame(updateTime);
updateTime();



function GameObject(spritesheet, x, y, width, height) 
{
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
 
}

 
let player = new GameObject(keyImage, posX, posY, playerSize, playerSize);
console.log(player);

let dino = new GameObject(dinoImage, dinoPosX, dinoPosY, dinoSizeX, dinoSizeY);

//494 x 1060


function imageLoader()
{  
    keyImage.src = "assets/images/key.png"
    dinoImage.src = "assets/images/dino.png"
    
   
    keyImage.onload = function ()
    {
       context.drawImage(keyImage, posX, posY, playerSize, playerSize);  
    } 
    dinoImage.onload = function ()
    {
        context.drawImage(dinoImage, 0, 0, 494, 1060, dinoPosX, dinoPosY, dinoSizeX, dinoSizeY);
    }
}
imageLoader();

function PlayerInput(input) 
{
    this.action = input; 
}

window.requestAnimationFrame(gameLoop);

function gameLoop()
{
    update();
    draw();
    window.requestAnimationFrame(gameLoop);
}




function input(event)
{
    // MOVEMENT
    if (event.type === "keydown") 
    {
        switch (event.keyCode)
        {
            case 37: // Left Arrow
                playerInput = new PlayerInput("Left");
                break; //Left key
            case 38: // Up Arrow
                playerInput = new PlayerInput("Up");
                break; //Up key
            case 39: // Right Arrow
                playerInput = new PlayerInput("Right");
                break; //Right key
            case 40: // Down Arrow
                playerInput = new PlayerInput("Down");
                break; //Down key
            case 32:
                playerInput = new PlayerInput("Space");
                break;
            case 82:
                playerInput = new PlayerInput("R");
                break;
            case 81:
                playerInput = new PlayerInput("Q");
                break;
            case 87:
                playerInput = new PlayerInput("W");
                break;
            case 69:
                 playerInput = new PlayerInput("E");
                break;
            case 82:
                playerInput = new PlayerInput("R");
                break;
            case 84:
                playerInput = new PlayerInput("T");
                break;
            case 89:
                playerInput = new PlayerInput("Y");
                break;   
           
            default:
                playerInput = new PlayerInput("None"); //No Input
        }
    } 

else
{
    playerInput = new PlayerInput("None"); //No Input
}

    
  

}



function draw()
{
    context.clearRect(0, 0, canvas.width, canvas.height);

    
        context.drawImage(player.spritesheet, 
            player.x,
            player.y,
            playerSize,
            playerSize);

        context.drawImage(dinoImage, 0, 0, 494, 1060, dinoPosX, dinoPosY, dinoSizeX, dinoSizeY);
        
    

   // context.drawImage(player.spritesheet, 
     //   player.x,
     //   player.y,
      //  playerSize,
      //  playerSize);
    
    

}

function update()
{
   move();
   events();

   
}

let speed = 3;
function move()
{
    

    if(playerInput.action === "Up")
    {
        player.y -= speed;
        console.log("UP");
    }
    if(playerInput.action === "Down")
    {
        console.log("Move Down");
        player.y += speed
    }
    if(playerInput.action === "Left")
    {
        console.log("Move Left");
        player.x -= speed
    }
    if(playerInput.action === "Right")
    {
        console.log("Move Right");
        player.x += speed
    }
    if(playerInput.action === "Space")
    {
        playerSize++;
    }
  
       
    
}

function events()
{
    let gameTimer = new Date();


    if(playerInput.action === "Q")
    {  
        document.getElementById("theCanvas").style.backgroundColor = "mediumvioletred";
    }
    if(playerInput.action === "W")
    {       
        document.getElementById("theCanvas").style.backgroundColor = "springgreen";
    }
    if(playerInput.action === "E")
    {       
        document.getElementById("theCanvas").style.backgroundColor = "tan";
    }
    if(playerInput.action === "R")
    {       
        document.getElementById("theCanvas").style.backgroundColor = "tomato";
    }
    if(playerInput.action === "T")
    {       
        document.getElementById("theCanvas").style.backgroundColor = "rgb(72, 141, 180)";  
    }
    if(playerInput.action === "Y")
    {       
        document.getElementById("theCanvas").style.backgroundColor = "rgb(15, 102, 37)";
    }
}